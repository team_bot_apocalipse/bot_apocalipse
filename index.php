<?php

include 'Telegram.php';

/* BOT TOKEN gerado pelo telegram*/
$bot_token = '714735262:AAH3V0cF3BU5vR-0fZNWhgCzJM424kWud10';
$telegram = new Telegram($bot_token);
$text = $telegram->Text();

if($text == 'Algum asteroide oferece perigo ao planeta Terra na data de hoje?'){


	$usersTimezone = 'America/Sao_Paulo';
	$date = new DateTime( 'now', new DateTimeZone($usersTimezone) );
	
	$today = $date->format('Y-m-d');
	$request_neows = 'https://api.nasa.gov/neo/rest/v1/feed?start_date='. $today .'&end_date='. $today .'&api_key=33M1WA0PWv1TFvlB65RoVoHFOQGMl8gEgMYu2NWR';

	$data = json_decode( file_get_contents($request_neows) );

	$near =  $data->near_earth_objects->$today;
	
	$return = '';
	foreach( $near as $value ){
		if($value->is_potentially_hazardous_asteroid){
			$return .= '<br /><br />';
			$return .= 'Nome: '. $value ->name;
			$return .= '<br />';
			$return .= 'Diâmetro estimado: '. ($value ->estimated_diameter->kilometers->estimated_diameter_min) .' Km à ' . ($value ->estimated_diameter->kilometers->estimated_diameter_max) .' Km';
			$return .= '<br />';
			$return .= 'Distância em que ele irá passar em relação à Terra: '. intVal($value ->close_approach_data[0]->miss_distance->kilometers).' Km';
			$return .= '<br />';
			$return .= 'Velocidade relativa à Terra: '. intVal($value ->close_approach_data[0]->relative_velocity->kilometers_per_hour).' Km';
			$return .= '<br />';
			$return .= $value ->nasa_jpl_url;
		}
	};
	if($return != ''){
		$emoticons = "\xF0\x9F\x98\xA8";
		$return = 'SIM! '. json_decode('"'.$emoticons.'"') .$return;
	} else {
		$emoticons = "\xF0\x9F\x98\x89";
		$return = 'Não, fiquetranquilo '. json_decode('"'.$emoticons.'"');
	}


	$return = str_replace('<br />',PHP_EOL,$return);
	

	$chat_id = $telegram->ChatID();


	$content = array('chat_id' => $chat_id, 'text' => $return);
	$telegram->sendMessage($content);
}

?>